WebDeveloper.storage = {};
WebDeveloper.storage.del = function ( name, tabid ) {
	if (typeof tabid != 'undefined') {
		delete localStorage[tabid+"#"+name];
    console.log('[storage][del] key "'+name+'" has been deleted for the tab: '+((''+tabid!="undefined")?tabid:"settings"));
	} else chrome.tabs.getSelected(null, function(tab) {
		delete localStorage[tab.id+"#"+name];
    console.log('[storage][del] key "'+name+'" has been deleted for the tab: '+((''+tabid!="undefined")?tabid:"current"));
	});
};

WebDeveloper.storage.set = function ( name, content, tabid ) {
	if (typeof tabid != 'undefined') {
		localStorage[tabid+"#"+name] = JSON.encode(content);
    console.log('[storage][set] key "'+name+'" has been set with the value: '+content+' for the tab: '+((''+tabid!="undefined")?tabid:"settings"));
	} else chrome.tabs.getSelected(null, function(tab) {
		localStorage[tab.id+"#"+name] = JSON.encode(content);
    console.log('[storage][set] key "'+name+'" has been set with the value: '+content+' for the tab: '+((''+tabid!="undefined")?tabid:"current"));
	});
};

WebDeveloper.storage.get = function ( name, safe, tabid ) {
	if (typeof tabid == 'undefined') tabid = "options";
	key = tabid + "#" + name;
  console.log( '[storage][get] Requested key: '+name+' for the tab: '+((''+tabid!="undefined")?tabid:"current") );

	if ( new String(localStorage[key]).blank() || localStorage[key] == null )
	{
		console.warn("[storage][get] content for key '"+name+"' is missing. for the tab: "+((''+tabid!="undefined")?tabid:"settings"));
		return;
	}
	
	if (typeof safe == 'undefined' || new String(safe).blank() || !safe)
	{
		var data = eval("("+localStorage[key]+")")
		if ( (new String(data)).blank() )
			return false;
		return data;
	}

	if ((new String(localStorage[key])).indexOf("\n") != -1)
	{
		var data = new String(localStorage[key]).replace(/\n/g, "///EOL///");
		if ( new String(data).blank() )
			return false;
		return eval("("+data+")").replace(/\/\/\/EOL\/\/\//g, "\n");
	}
	
	var data = eval("("+localStorage[key]+")");

	if ( new String(data).blank() )
		return false;
		
	return new String(data);
};

WebDeveloper.storage.get_all = function ( name, safe, tabid ) {
	if (typeof tabid == 'undefined') tabid = "options";
	var ret = {};
	for (var key in localStorage)
	   if ( key.split("#")[0] == (tabid+''))
	     ret[key.split("#")[1]] = localStorage[key];

  console.log( '[storage][get_all] Requested all "'+name+'" values for the tab: '+((''+tabid!="undefined")?tabid:"current") );
	return ret;
};


WebDeveloper.storage.keys = function ( tabid ) {
	if (typeof tabid == 'undefined') tabid = "option";
	var keys = [];
	for ( var key in localStorage )
		if ((key.split("#")[0]+'') == (tabid+''))
			keys.push(key.split("#")[1]+'');

	console.log( '[storage][keys] Found +'+(keys.length+'')+'+ all keys for the tab: '+((''+tabid!="undefined")?tabid:"settings") );
	return keys
};

WebDeveloper.storage.reset = function ( tabid ) {
	var id = (tabid+'');
	for (var i in localStorage)
		if ( typeof tabid == 'undefined' || new String(i).split("#")[0] == id )
			delete localStorage[i];
			
	console.log( '[storage][reset] Deleted all the keys for the tab: '+((''+tabid!="undefined")?tabid:"settings") );
};

WebDeveloper.storage.reset_current_tab = function ( name, content ) {
	chrome.tabs.getSelected(null, function(tab) {
		WebDeveloper.storage.reset(tab.id);
	});
};

WebDeveloper.get = WebDeveloper.storage.get;
WebDeveloper.set = WebDeveloper.storage.set;
WebDeveloper.del = WebDeveloper.storage.del;
WebDeveloper.get_all = WebDeveloper.storage.get_all;
WebDeveloper.keys = WebDeveloper.storage.keys;
WebDeveloper.reset = WebDeveloper.storage.reset;
WebDeveloper.reset_current_tab = WebDeveloper.storage.reset_current_tab;