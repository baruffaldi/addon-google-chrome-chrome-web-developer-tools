WebDeveloper.badgeColors = {
  'red': [250, 3, 3, 140],
  'purple': [180, 40, 255, 140],
  'green': [36, 140, 87, 140],
  'magenta': [255, 153, 180, 140],
  'blue': [3, 97, 255, 132],
  'red_transparent': [250, 3, 3, 73],
  'purple_transparent': [180, 2, 255, 73],
  'green_transparent': [36, 140, 87, 73],
  'magenta_transparent': [255, 153, 180, 73],
  'blue_transparent': [3, 97, 255, 73]
};

WebDeveloper.badgeIcons = [
	"code.png",
	"configuration.png",
	"default_badge.png",
	"default_mech.png",
	"head.png",
  "nes.png",
	"mech.png",
  "rulers.png",
	"source.png",
	"web_developer_tools_logo.png",
	"xconsole.png"
]

WebDeveloper.backgrounds = [
	"default.jpg",
	"options.jpg"
]

WebDeveloper.themes = {
    'default': [
		["#eee",  // options color
		 "#fff",  // options h1 color
		 "2px solid #ccc", // borders
		 "#aaa"], // background-color
		0, // optionsBackground
		3, // badgeIcon
		"", // badgeText
		"red"  // badgeColor
	],
    'test': [
		["#eee",  // options color
		 "#fff",  // options h1 color
		 "2px solid #ccc", // borders
		 "2px solid #ccc", // borders-shadow
		 "#aaa"], // background-color
		1, // optionsBackground
		6, // badgeIcon
		"mdb",   // badgeText
		"purple"  // badgeColor
	],
    'code': [
		["#eee",  // options color
		 "#fff",  // options h1 color
		 "2px solid #ccc", // borders
		 "#aaa"], // background-color
		1, // optionsBackground
		5, // badgeIcon
		"code",   // badgeText
		"red"  // badgeColor
	]
};

WebDeveloper.init_theme = function() {
  if (!this.storage) return;
  this.theme = WebDeveloper.get("wd_theme") || 'default';
  this.css = WebDeveloper.get("wd_theme_css") || WebDeveloper.themes[this.theme][0];
  this.badgeText = WebDeveloper.get("wd_badge_text") || "";
  this.badgeColor = WebDeveloper.get("wd_badge_color") || this.themes[this.theme][4];
  this.badgeColorName = WebDeveloper.get("wd_badge_color_name") || this.badgeColor;
  this.badgeColorVector = WebDeveloper.get("wd_badge_color_vector") || this.badgeColors[this.badgeColor];
  this.badgeIcon = WebDeveloper.get("wd_badge_icon") || this.badgeIcons[this.themes[this.theme][2]];
  this.optionsBackground = WebDeveloper.get("wd_options_background") || this.backgrounds[this.themes[this.theme][1]];
  this.update_theme(true)
  console.log( "[themes][init_theme] Theme update finished." );
};

WebDeveloper.update_theme = function(skip) {
  if (!this.storage) return;
  
  this.theme = WebDeveloper.get("wd_theme", false, "options") || 'default';
  this.badgeText = WebDeveloper.get("wd_badge_text", false, "options") || "";
  this.badgeIcon = WebDeveloper.get("wd_badge_icon") || this.badgeIcons[this.themes[this.theme][2]];
  this.badgeColor = WebDeveloper.get("wd_badge_color") || this.themes[this.theme][4];
  this.badgeColorName = WebDeveloper.get("wd_badge_color_name") || this.badgeColor;
  this.badgeColorVector = WebDeveloper.get("wd_badge_color_vector", false, "options") || this.badgeColors[this.badgeColor];
  
  WebDeveloper.setBrowserIcon( this.badgeIcon, "badgeicons/" );
  chrome.browserAction.setBadgeText({text:this.badgeText});
  chrome.browserAction.setBadgeBackgroundColor({'color':this.badgeColorVector});
  if (!skip+'')
    console.log( "[themes][update_theme] Theme update finished." );
};

WebDeveloper.setBrowserIcon = function (name, path) 
{
	path = "images/" + path + name;
	var img = new Image();
	
	img.onerror = function() {
		console.error("[themes][setBrowserIcon]Could not load image '" + path + name +"'.");
	}
	img.onload = function() {
		var canvas = document.createElement("canvas");
		canvas.width = img.width > 19 ? 19 : img.width;
		canvas.height = img.height > 19 ? 19 : img.height;

		var canvas_context = canvas.getContext('2d');
		canvas_context.clearRect(0, 0, canvas.width, canvas.height);
		canvas_context.drawImage(img, 0, 0, canvas.width, canvas.height);
		var imgData = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
		chrome.browserAction.setIcon({imageData: imgData});
		delete imgData, canvas_context;
	}
	
	img.src = path;
	console.log( "[themes][setBrowserIcon] The browser action icon: "+path+" has been set." );
	delete canvas, img;
};
  
WebDeveloper.setPageIcon = function (name, path) 
{
  path = "images/" + path + name;
  var img = new Image();
  img.onerror = function() {
    console.error("[themes][setPageIcon] Could not load image '" + path + name +"'.");
  }
  img.onload = function() {
    var canvas = document.createElement("canvas");
    canvas.width = img.width > 19 ? 19 : img.width;
    canvas.height = img.height > 19 ? 19 : img.height;

    var canvas_context = canvas.getContext('2d');
    canvas_context.clearRect(0, 0, canvas.width, canvas.height);
    canvas_context.drawImage(img, 0, 0, canvas.width, canvas.height);
    var imgData = canvas_context.getImageData(0, 0, canvas.width, canvas.height);
    chrome.browserAction.setIcon({imageData: imgData});
    console.log( "[themes][setPageIcon] The page action icon: "+path+" has been set." );
    return imgData;
  }
};