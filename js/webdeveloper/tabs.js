WebDeveloper.tabs = {};
WebDeveloper.tabs.inject = {};

WebDeveloper.tabs.inject.JS = function(t, id, script, toClose){
  chrome.tabs.getSelected(t?t:null, function(tab) {
    if (!WebDeveloper.isUrl(tab.url)) return;
    var js_file = {};
    var bkg = chrome.extension.getBackgroundPage();
    if (!bkg.WD.get(id, false, tab.id)) {
      js_file.file = chrome.extension.getURL('js/inject/'+script+'_on.js');
      chrome.tabs.executeScript(tab.id, js_file, function() {
        bkg.WD.set(id, true, tab.id);
        console.log('Filter "'+id+'" has been applied to the tab: '+str(tab.id)+' trougth the script inclusion: '+script);
        if (toClose+'') window.close();
      });
    } else {
      js_file.file = chrome.extension.getURL('js/inject/'+script+'_off.js');
      chrome.tabs.executeScript(tab.id, js_file, function() {
        bkg.WD.del(id, tab.id);
        console.log('Filter "'+id+'" has been removed to the tab: '+str(tab.id)+' trougth the script inclusion: '+script);
        if (toClose+'') window.close();
      });
    }
  });
};