$(document).ready(function(){
	/*
	 *  DISABLE SECTION
	 */
  $('#disable_css').click(function(){
    //WD.tabs.inject.JS(null, 'wd_css_disable', 'css_disable_all', false);
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_disable_css', false, tab.id)) {
        js_file.file = 'js/inject/disable_css_all_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_disable_css', true, tab.id);
          
            $("#disable_css").toggleClass("active_filter");
            console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/disable_css_all_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_disable_css', tab.id);
		      
		        $("#disable_css").toggleClass("active_filter");
		        console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#disable_browser_css').click(function(){
    //WD.tabs.inject.JS(null, 'wd_css_disable', 'css_disable_all', false);
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_disable_browser_css', false, tab.id)) {
        js_file.file = 'js/inject/disable_browser_css_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_disable_browser_css', true, tab.id);
          
            $("#disable_browser_css").toggleClass("active_filter");
            console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/disable_browser_css_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_disable_browser_css', tab.id);
          
            $("#disable_browser_css").toggleClass("active_filter");
            console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
	$('#disable_images').click(function(){
    //WD.tabs.inject.JS(null, 'wd_images_disable_all', 'images_disable_all', false);
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_disable_images', false, tab.id)) {
				js_file.file = 'js/inject/disable_images_all_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_disable_images', true, tab.id);
          
            $("#disable_images").toggleClass("active_filter");
            console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/disable_images_all_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_disable_images', tab.id);
          
            $("#disable_images").toggleClass("active_filter");
            console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#disable_colors').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_disable_colors', false, tab.id)) {
				js_file.file = 'js/inject/disable_colors_all_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_disable_colors', true, tab.id);
          
            $("#disable_colors").toggleClass("active_filter");
            console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/disable_colors_all_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_disable_colors', tab.id);
          
            $("#disable_colors").toggleClass("active_filter");
            console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	
	/*
	 *  IMAGES SECTION
	 */
  $('#images_disable').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_images_disable', false, tab.id)) {
        js_file.file = 'js/inject/images_disable_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_images_disable', true, tab.id);
          
            $("#images_disable").toggleClass("active_filter");
            console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/images_disable_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_images_disable', tab.id);
          
            $("#images_disable").toggleClass("active_filter");
            console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
	$('#images_disable_backgrounds').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_images_disable_backgrounds', false, tab.id)) {
				js_file.file = 'js/inject/images_disable_bgs_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_images_disable_backgrounds', true, tab.id);
          
          $("#images_disable_backgrounds").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/images_disable_bgs_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_images_disable_backgrounds', tab.id);
          
          $("#images_disable_backgrounds").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#images_remove').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_images_remove', false, tab.id)) {
				js_file.file = 'js/inject/images_remove_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_images_remove', true, tab.id);
          
          $("#images_remove").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/images_remove_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_images_remove', tab.id);
          
          $("#images_remove").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#images_remove_backgrounds').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_images_remove_backgrounds', false, tab.id)) {
				js_file.file = 'js/inject/images_remove_bgs_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_images_remove_backgrounds', true, tab.id);
          
          $("#images_remove_backgrounds").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/images_remove_bgs_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_images_remove_backgrounds', tab.id);
          
          $("#images_remove_backgrounds").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#images_remove_all').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_images_remove_all', false, tab.id)) {
				js_file.file = 'js/inject/images_remove_all_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_images_remove_all', true, tab.id);
          
          $("#images_remove_all").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/images_remove_all_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_images_remove_all', tab.id);
          
          $("#images_remove_all").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
  $('#images_show_without_alts').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_images_show_without_alts', false, tab.id)) {
        js_file.file = 'js/inject/images_show_noalts_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_images_show_without_alts', true, tab.id);
          
          $("#images_show_without_alts").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/images_show_noalts_off.js.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_images_show_without_alts', tab.id);
          
          $("#images_show_without_alts").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#images_show_without_title').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_images_show_without_title', false, tab.id)) {
        js_file.file = 'js/inject/images_show_notitle_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_images_show_without_title', true, tab.id);
          
          $("#images_show_without_title").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/images_show_notitle_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_images_show_without_title', tab.id);
          
          $("#images_show_without_title").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#images_show_without_dim').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_images_show_without_dim', false, tab.id)) {
        js_file.file = 'js/inject/images_show_nodim_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_images_show_without_dim', true, tab.id);
          
          $("#images_show_without_dim").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/images_show_nodim_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_images_show_without_dim', tab.id);
          
          $("#images_show_without_dim").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#images_make_transparent').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_images_make_transparent', false, tab.id)) {
        js_file.file = 'js/inject/images_make_transp_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_images_make_transparent', true, tab.id);
          
          $("#images_make_transparent").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/images_make_transp_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_images_make_transparent', tab.id);
          
          $("#images_make_transparent").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  
	$('#images_show_alts').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_images_show_alts', false, tab.id)) {
				js_file.file = 'js/inject/images_show_alts_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_images_show_alts', true, tab.id);
          
          $("#images_show_alts").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/images_show_alts_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_images_show_alts', tab.id);
          
          $("#images_show_alts").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#images_show_dimensions').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_images_show_dimensions', false, tab.id)) {
				js_file.file = 'js/inject/images_show_dimensions_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_images_show_dimensions', true, tab.id);
          
          $("#images_show_dimensions").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/images_show_dimensions_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_images_show_dimensions', tab.id);
          
          $("#images_show_dimensions").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
  $('#images_show_oversized').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_images_show_oversized', false, tab.id)) {
        js_file.file = 'js/inject/images_show_oversized_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_images_show_oversized', true, tab.id);
          
          $("#images_show_oversized").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/images_show_oversized_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_images_show_oversized', tab.id);
          
          $("#images_show_oversized").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#images_show_source').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_images_show_source', false, tab.id)) {
        js_file.file = 'js/inject/images_show_source_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_images_show_source', true, tab.id);
          
          $("#images_show_source").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/images_show_source_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_images_show_source', tab.id);
          
          $("#images_show_source").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
	
	/*
	 *  OUTLINE SECTION
	 */
	$('#outline_tables').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_outline_tables', false, tab.id)) {
				js_file.file = 'js/inject/outline_tables_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_outline_tables', true, tab.id);
          
          $("#outline_tables").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/outline_tables_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_outline_tables', tab.id);
          
          $("#outline_tables").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#outline_tables_cells').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_outline_tables_cells', false, tab.id)) {
				js_file.file = 'js/inject/outline_tables_cells_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_outline_tables_cells', true, tab.id);
          
          $("#outline_tables_cells").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/outline_tables_cells_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_outline_tables_cells', tab.id);
          
          $("#outline_tables_cells").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#outline_divs').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_outline_divs', false, tab.id)) {
				js_file.file = 'js/inject/outline_divs_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_outline_divs', true, tab.id);
          
          $("#outline_divs").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/outline_divs_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_outline_divs', tab.id);
          
          $("#outline_divs").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#outline_paragraphs').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_outline_paragraphs', false, tab.id)) {
				js_file.file = 'js/inject/outline_paragraphs_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_outline_paragraphs', true, tab.id);
          
          $("#outline_paragraphs").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/outline_paragraphs_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_outline_paragraphs', tab.id);
          
          $("#outline_paragraphs").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#outline_lists').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_outline_lists', false, tab.id)) {
				js_file.file = 'js/inject/outline_lists_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_outline_lists', true, tab.id);
          
          $("#outline_lists").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/outline_lists_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_outline_lists', tab.id);
          
          $("#outline_lists").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#outline_images').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_outline_images', false, tab.id)) {
				js_file.file = 'js/inject/outline_images_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_outline_images', true, tab.id);
          
          $("#outline_images").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/outline_images_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_outline_images', tab.id);
          
          $("#outline_images").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
	$('#outline_headings').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			var bkg = chrome.extension.getBackgroundPage();
			if (!bkg.WD.get('wd_outline_headings', false, tab.id)) {
				js_file.file = 'js/inject/outline_headings_on.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.set('wd_outline_headings', true, tab.id);
          
          $("#outline_headings").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
					//window.close();
				});
			} else {
				js_file.file = 'js/inject/outline_headings_off.js';
				chrome.tabs.executeScript(tab.id, js_file, function() {
					bkg.WD.del('wd_outline_headings', tab.id);
          
          $("#outline_headings").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
					//window.close();
				});
			}
		});
	});
  $('#outline_span').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_outline_span', false, tab.id)) {
        js_file.file = 'js/inject/outline_span_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_outline_span', true, tab.id);
          
          $("#outline_span").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/outline_span_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_outline_span', tab.id);
          
          $("#outline_span").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#outline_forms').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_outline_forms', false, tab.id)) {
        js_file.file = 'js/inject/outline_forms_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_outline_forms', true, tab.id);
          
          $("#outline_forms").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/outline_forms_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_outline_forms', tab.id);
          
          $("#outline_forms").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#outline_frames').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_outline_frames', false, tab.id)) {
        js_file.file = 'js/inject/outline_frames_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_outline_frames', true, tab.id);
          
          $("#outline_frames").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/outline_frames_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_outline_frames', tab.id);
          
          $("#outline_frames").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#outline_anchors').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_outline_anchors', false, tab.id)) {
        js_file.file = 'js/inject/outline_anchors_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_outline_anchors', true, tab.id);
          
          $("#outline_anchors").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/outline_anchors_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_outline_anchors', tab.id);
          
          $("#outline_anchors").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#outline_corr_elements').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_outline_corr_elements', false, tab.id)) {
        js_file.file = 'js/inject/outline_corr_elems_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_outline_corr_elements', true, tab.id);
          
          $("#outline_corr_elements").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/outline_corr_elems_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_outline_corr_elements', tab.id);
          
          $("#outline_corr_elements").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#outline_block_level_elements').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_outline_block_level_elements', false, tab.id)) {
        js_file.file = 'js/inject/outline_block_elems_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_outline_block_level_elements', true, tab.id);
          
          $("#outline_block_level_elements").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/outline_block_elems_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_outline_block_level_elements', tab.id);
          
          $("#outline_block_level_elements").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#outline_deprecated_elements').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_outline_deprecated_elements', false, tab.id)) {
        js_file.file = 'js/inject/outline_depr_elems_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_outline_deprecated_elements', true, tab.id);
          
          $("#outline_deprecated_elements").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/outline_depr_elems_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_outline_deprecated_elements', tab.id);
          
          $("#outline_deprecated_elements").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  
  /*
   *  SHOW SECTION
   */
  $('#show_links_without_title_attr').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_links_without_title_attr', false, tab.id)) {
        js_file.file = 'js/inject/show_links_notitle_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_links_without_title_attr', true, tab.id);
          
          $("#show_links_without_title_attr").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_links_notitle_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_links_without_title_attr', tab.id);
          
          $("#show_links_without_title_attr").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  
  /*
   *  SHOW SECTION
   */
  $('#show_links_without_title_attributes').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_links_without_title_attributes', false, tab.id)) {
        js_file.file = 'js/inject/show_links_notitle_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_links_without_title_attributes', true, tab.id);
          
          $("#show_links_without_title_attributes").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_links_notitle_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_links_without_title_attributes', tab.id);
          
          $("#show_links_without_title_attributes").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#show_links_with_ping_attributes').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_links_with_ping_attributes', false, tab.id)) {
        js_file.file = 'js/inject/show_links_ping_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_links_with_ping_attributes', true, tab.id);
          
          $("#show_links_with_ping_attributes").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_links_ping_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_links_with_ping_attributes', tab.id);
          
          $("#show_links_with_ping_attributes").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#show_links_details').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_links_details', false, tab.id)) {
        js_file.file = 'js/inject/show_links_details_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_links_details', true, tab.id);
          
          $("#show_links_details").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_links_details_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_links_details', tab.id);
          
          $("#show_links_details").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#show_forms_details').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_forms_details', false, tab.id)) {
        js_file.file = 'js/inject/show_forms_details_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_forms_details', true, tab.id);
          
          $("#show_forms_details").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_forms_details_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_forms_details', tab.id);
          
          $("#show_forms_details").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#show_abbreviations').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_abbreviations', false, tab.id)) {
        js_file.file = 'js/inject/show_abbr_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_abbreviations', true, tab.id);
          
          $("#show_abbreviations").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_abbrs_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_abbreviations', tab.id);
          
          $("#show_abbreviations").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#show_screen_rendering').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_screen_rendering', false, tab.id)) {
        js_file.file = 'js/inject/show_scr_rendering_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_screen_rendering', true, tab.id);
          
          $("#show_screen_rendering").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_scr_rendering_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_screen_rendering', tab.id);
          
          $("#show_screen_rendering").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#show_linearized_page').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_linearized_page', false, tab.id)) {
        js_file.file = 'js/inject/show_linearized_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_linearized_page', true, tab.id);
          
          $("#show_linearized_page").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_linearized_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_linearized_page', tab.id);
          
          $("#show_linearized_page").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });
  $('#show_topographic_informations').click(function(){
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_show_topographic_informations', false, tab.id)) {
        js_file.file = 'js/inject/show_topo_info_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_show_topographic_informations', true, tab.id);
          
          $("#show_topographic_informations").toggleClass("active_filter");
          console.log("[browser_action][filter_apply] Filter request");
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/show_topo_info_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_show_topographic_informations', tab.id);
          
          $("#show_topographic_informations").toggleClass("active_filter");
          console.log("[browser_action][filter_remove] Filter request");
          //window.close();
        });
      }
    });
  });

	/*
	 *  VALIDATE SECTION
	 */
	$('#validate_html').click(function(){
		chrome.tabs.getSelected( null, function(tab) {
				chrome.tabs.create({url: 'http://validator.w3.org/check?uri='+tab.url+'&amp;doctype=Inline'});
		});
		console.log('Validating HTML: '+tab.url);
	});
	$('#validate_css').click(function(){
		chrome.tabs.getSelected( null, function(tab) {
			chrome.tabs.create({url: 'http://jigsaw.w3.org/css-validator/validator?uri='+tab.url});
		});
		console.log('Validating CSS: '+tab.url);
	});
	$('#validate_links').click(function(){
		chrome.tabs.getSelected( null, function(tab) {
			chrome.tabs.create({url: 'http://validator.w3.org/checklink?uri='+tab.url});
		});
		console.log('Validating Links: '+tab.url);
	});
	$('#validate_feed').click(function(){
		chrome.tabs.getSelected( null, function(tab) {
			chrome.tabs.create({url: 'http://validator.w3.org/feed/check.cgi?url='+tab.url});
		});
		console.log('Validating Feed: '+tab.url);
	});
	$('#validate_508').click(function(){
		chrome.tabs.getSelected( null, function(tab) {
			chrome.tabs.create({url: 'http://www.cynthiasays.com/mynewtester/cynthia.exe?rptmode=-1&url1='+tab.url});
		});
		console.log('Validating 508: '+tab.url);
	});
	$('#validate_wai').click(function(){
		chrome.tabs.getSelected( null, function(tab) {
			chrome.tabs.create({url: 'http://www.cynthiasays.com/mynewtester/cynthia.exe?rptmode=2&url1='+tab.url});
		});
		console.log('Validating WAI: '+tab.url);
	});
	
	/*
	 *  SEO TOOLS SECTIONS
	 */
	$('#check_pagerank').click(function(){
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			js_file.file = 'js/inject/seo_check_pagerank.js';
			chrome.tabs.executeScript(tab.id, js_file, function() {
				//window.close();
			});
		});
	});
	
	/*
	 *  OTHERS SECTIONS
	 */
	$('#browser').click(function(){
		chrome.tabs.getSelected(
			null,
			function(tab) {
				chrome.tabs.create({url: 'http://www.browsershots.org/?url='+tab.url});
			}
		);
		console.log('Creating snapshots for URL: '+tab.url);
	});
	$('#edit_css').click(function(){
		/*
		- inject html code per inserire un bel textarea subito sotto alla UI
		- on change
		-- clean all styles
		-- impostare il nuovo style
		*/
		chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_edit_hml', false, tab.id)) {
        js_file.file = 'js/inject/edit_hml_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_edit_hml', true, tab.id);
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/edit_hml_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_edit_hml', tab.id);
          //window.close();
        });
      }
    });
	});
	$('#edit_html').click(function(){
		/*
		- inject html code per inserire un bel textarea subito sotto alla UI
		- on change
		-- impostare il nuovo codice html ( innerHtml )
		*/
    chrome.tabs.getSelected(null, function(tab) {
      if (!WD.isUrl(tab.url)) return;
      var js_file = {};
      var bkg = chrome.extension.getBackgroundPage();
      if (!bkg.WD.get('wd_edit_hml', false, tab.id)) {
        js_file.file = 'js/inject/edit_hml_on.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.set('wd_edit_hml', true, tab.id);
          //window.close();
        });
      } else {
        js_file.file = 'js/inject/edit_hml_off.js';
        chrome.tabs.executeScript(tab.id, js_file, function() {
          bkg.WD.del('wd_edit_hml', tab.id);
          //window.close();
        });
      }
    });
	});
	$('#switch_user_agent').click(function(){
		/*
		- inject html code per inserire un bel select subito sotto alla UI
		- on change submit
		-- impostare il nuovo useragent
		-- chiudere la finestrella
		-- alert con 2 tasti, annulla o ok: ricaricare la pagina corrente
		*/
		chrome.tabs.getSelected(null, function(tab) {
			if (!WD.isUrl(tab.url)) return;
			var js_file = {};
			js_file.file = 'js/inject/seo_check_pagerank.js';
			chrome.tabs.executeScript(tab.id, js_file, function() {
				//window.close();
			});
		});
	});
	
	/*
	 *  OTHERS SECTIONS
	 */
	$('#options').click(function(){
		chrome.tabs.create({url: 'options.html'});
	});
	
	$('#close_popup').click(function(){window.close()});
	
	/*
	 *  ANIMATIONS AND EFFECTS
	 */
	$('dd.popup_list dl').hide();
  $('h2.menu_entry').each(function(i, el){
    $(el).click(function(){
      $('dd.popup_list dl').slideUp(500);
      $('h2.menu_entry_active').each(function(i, e){
        filename = $(e).children("img").attr("src").split("/").pop();
        $(e).toggleClass("menu_entry").toggleClass("menu_entry_active")
        .children("img").attr("width","16");
      });
      if ($(el).hasClass("menu_entry")) {
        $(el).next().slideDown(1000);
        $(el).toggleClass("menu_entry").toggleClass("menu_entry_active");
        filename = $(el).children("img").attr("src").split("/").pop();
        $(el).children("img").attr("width","24");
      }
    });
  });
  $('a.row_without_icon span').each(function(i, el){
    $(el).css('padding-left', '24px');
  });
	
	$('h2:last').css('border-bottom', '1px solid #e0e0e0');

	$('dd a').each(function(i, el){
		$(el).mouseover(function(){
			$(this).children("img").removeClass("stop_rotate");
			$(this).children("img").addClass("rotate");
		}).mouseout(function(){
			$(this).children("img").removeClass("rotate");
			$(this).children("img").addClass("stop_rotate");
		});
	});
	
	chrome.tabs.getSelected(null, function(tab) {
	  if (!WD.isUrl(tab.url)) return;
	  $(WD.keys(tab.id)).each(function(i, el) {
	    var name = (el+'').substr(3);
	    if (document.getElementById(name)) {
	      $("#"+name).toggleClass("active_filter");
	    }
	  });
	});
});


/*
function updateRotation(){
	chrome.tabs.getSelected(null, function(tab) {
		if (!WD.isUrl(tab.url)) return;
		var bkg = chrome.extension.getBackgroundPage();
		
		for(var x in bkg.WD.get_all_keys(tab.id))
			$('.rotateloop').css("-webkit-transform", "rotate(360deg)");
		alert("AZ");
	});
}
window.setInterval(updateRotation,10000);*/