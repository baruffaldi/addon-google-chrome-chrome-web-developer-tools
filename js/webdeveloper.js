String.prototype.blank = String.prototype.blank || function(s) {
  if ( typeof s == 'undefined' )
    s = this;
  return ( new String(s).trim() == '' || typeof s == 'undefined' || typeof s == 'null' );
};

JSON.encode = JSON.encode || function(input){
    if (!input) return 'null'
    switch (input.constructor) {
      case String: return '"' + input + '"'
      case Number: return input.toString()
      case Boolean: return (input) ? "true" : "false"
      case Array:
        var buf = []
        for (i in input)
          buf.push(JSON.encode(input[i]))
            return '[' + buf.join(', ') + ']'
      case Object:
        var buf = []
        for (k in input)
          buf.push(k + ' : ' + JSON.encode(input[k]))
            return '{ ' + buf.join(', ') + '} '
      default:
        return 'null'
    }
};

var WebDeveloper = window.WebDeveloper || {};
var WD = WebDeveloper;

WebDeveloper.isUrl = function(url) {
  return (new String(url)).match(/^https?:\/\//i) ? true : false;
};

WebDeveloper.version = "0.1";