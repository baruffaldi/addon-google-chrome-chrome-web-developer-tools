chrome.tabs.onUpdated.addListener(function(tabId, props, tab) {
  if (props.status == 'complete') WD.reset(tabId);
});
chrome.tabs.onRemoved.addListener(function(tabId) {
  WD.reset(tabId);
});