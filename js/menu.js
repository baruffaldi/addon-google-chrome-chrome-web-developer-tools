var menu = [{
	'name': "Disable",
	'entries': [
		["disable_css", "Disable Site CSS", "css_disable.png"],
		["disable_browser_css", "Disable Browser CSS", "css_disable.png"],
		["disable_images", "Disable Images", "images.png"],
		["disable_colors", "Disable Colors", "colors.png"]/*,
		["disable_cookies", "Disable Cookies", "cookies.png"]*/
	]},{
	'name': "Images",
	'entries': [
		["images_disable", "Hide Images"],
		["images_disable_backgrounds", "Hide Backgrounds"],
		["images_remove", "Remove Images"],
		["images_remove_backgrounds", "Remove Backgrounds"],
    ["images_remove_all", "Remove Both"],
    ["images_make_transparent", "Transform all to Transparent"],
    ["images_show_without_alts", "Images without alt Attr"],
    ["images_show_without_title", "Images without title Attr"],
    ["images_show_without_dim", "Images without dimensions Attr"]/*,
		["", "Show Sizes"],
    ["images_show_oversized", "Oversized images"],
		["images_show_source", "Show Images Sources"],
    ["images_show_alts", "Images with alt Attr"],
		["images_show_dimensions", "Images with dimensions Attr"],
    ["images_adjusted_dim", "Adjusted dimension images"],
		["images_empty_alt", "Images with empty title"]*/
		
	]},{
	'name': "Outline",
	'entries': [
		["outline_tables", "Outline Tables"],
		["outline_tables_cells", "Outline Table Cells"],
		["outline_divs", "Outline Divs"],
		["outline_paragraphs", "Outline Paragraphs"],
		["outline_lists", "Outline Lists"],
		["outline_images", "Outline Images"],
		["outline_headings", "Outline Headings"],
		["outline_span", "Outline span Elements"],
    ["outline_forms", "Outline Forms"],
    ["outline_frames", "Outline Frames"],
    ["outline_anchors", "Outline Anchors"],
    ["outline_corr_elements", "Outline Corrupted Elements"],
    ["outline_block_level_elements", "Outline Block Level Elements"],/*
    ["outline_forms", "Outline Positioned Elements"],
    ["outline_anchors", "Outline Floated Elements"],*/
    ["outline_deprecated_elements", "Outline Deprecated Elements"]
	]},{
	'name': "Show",
	'entries': [
    ["show_links_without_title_attr", "Links without title Attr"],
    ["show_links_with_ping_attributes", "Links with ping Attr"],
    ["show_links_details", "Links details"],
    ["show_forms_details", "Forms details"],
    ["show_abbreviations", "Abbreviations"],
    ["show_screen_rendering", "Screen rendering"],
    ["show_linearized_page", "Linearized page"],
    ["show_topographic_informations", "Topographic informations"]/*,
		["show_window_dimensions", "Show Window Dimensions"],
		["show_layout_dimensions", "Show Layout Dimensions"],
		["show_dimensions_on_page_title", "Show Dimensions On Title[option]"],
		["show_zoom_in", "Zoom In"],
		["show_zoom_out", "Zoom Out"],
		["show_positioned_elements", "Positioned Elements"],
		["show_hidden_elements", "Show hidden Elements"]*/
	]},/*{
	'name': "Edit",
	'entries': [
		["edit_html", "Edit HTML"],
		["edit_css", "Edit CSS"]
	]},{
	'name': "Forms",
	'entries': [
		["", "Show Details"],
		["", "Show inputs Tags"],
		["show_hidden_elements", "Show hidden Inputs"],
		["", "Enable all Forms"],
		["", "Deselect all Radios"],
		["", "Show password Fields"],
		["", "Convert methods"],
		["", "Convert selects in texts"],
		["", "Disable maxlength"]
	]},{
	'name': "Cookies",
	'entries': [
		["", "View Cookies"],
		["", "Add Cookies"],
		["", "Delete Session Cookies"],
		["", "Delete Domain Cookies"],
		["", "Delete Path Cookies"]
	]},*/{
	'name': "Validate",
	'entries': [
		["validate_html", "Validate HTML"],
		["validate_css", "Validate CSS"],
		["validate_links", "Validate Links"],
		["validate_feed", "Validate Feed"],
		["validate_508", "Validate 508"],
		["validate_wai", "Validate WAI"]
	]},/*{
	'name': "SEO Tools",
	'entries': [
		["check_pagerank", "Check PageRank"]
	]},*/{
	'name': "Tools",
	'entries': [
		//["switch_user_agent", "UserAgent Switcher", "useragent.png"],
    ["browser", "XBrowser Snapshots", "screenshot.png"],
    ["console", "Chrome console"]
	]}
];
var popup = "<dl><dd style='height: 33px;'><img src='images/others/web_developer_tools_logo_small.png"+/*(WebDeveloper.get("wd_badge_icon")||"")*/"' alt='Web Developer' /><h1 style='display:inline;margin-left: 3px;'>Web Developer</h1></dd>";

var section=0;
while ( section<menu.length ) {
	popup += "<dd class='popup_list'><h2 class='menu_entry'><img src='images/16/"+menu[section]["name"].toLowerCase()+".png' alt='- ' />"+menu[section]["name"]+"</h2><dl class='popup_sub_list'>";
	var entries = menu[section]["entries"];
	for ( var entry in entries )
	{
		popup += "<dd class='popup_row'>";
			  popup +=  ((entries[entry][2])+'' != 'undefined')
			               ?("<a id='"+entries[entry][0]+"' href='#' title='"+entries[entry][1]+"'><img src='images/16/"+((entries[entry].length > 2)?entries[entry][2]:menu[section]["name"].toLowerCase()+".png")+"' alt='- ' />")
			               :("<a class='row_without_icon' id='"+entries[entry][0]+"' href='#' title='"+entries[entry][1]+"'>");
 
		popup += "<span>"+entries[entry][1]+"</span></a></dd>";
	}
	popup += "</dl></dd>";
	++section;
	delete e, entry, entries;
}

popup += "<dd><dl><dd><a id='options' href='#' title='Options'><img src='images/16/options.png' alt='- ' /><span>Options</span></a><a id='close_popup' href='#'><img src='images/16/close_window.png' alt='Close this popup' /></a></dd></dl></dd></dl>";

$(document.body).ready(function(){$(document.body).html(popup);});
delete popup, section, menu;