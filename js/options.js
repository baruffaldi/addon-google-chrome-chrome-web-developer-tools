/*
<!--<script type="text/javascript" src="js/themes.js"></script>
<script type="text/javascript" src="js/bg/theme.js"></script>
<script type="text/javascript" src="js/bg/useragent.js"></script>-->
- import useragents
- export useragents
- switch useragent ( tramite ricarica della pagina bloccando il caricamento normale da background )
- NPAPI per lo switch

- edit css ( usare InjectCSS di chrome.tabs )
- genera css
- edit html ( usare html() )

- pagina About Web Developer con link nel popup ( stile web developer )

- applet figo per l'assegnazione di colori custom
- aumentare la customizzazione dei temi ( aggiungendo tutte le immagini icone blah blah )
- usare icone e immagini del tema di default per quelle mancanti ai temi custom
- sincronizzare i colori del tema di chrome con quelli dell'estensione
- icone in bianco e nero quando non � stato usato il tasto, a colori quando � attivo e disattivabile a colori per tutte quelle fisse senza state

- comprimere il codice
- check v8
- sfruttare html5 e css3
*/

function import_useragents() {
    alert('a');
	
}

function export_useragents() {
	alert('b');
}

function report(msg) {
	init();
	$('#optionsResultBox').html(msg);
	$('#optionsResultBox').fadeIn(1500);
	window.setTimeout(function(){$('#optionsResultBox').fadeOut(2000);}, 2500);
}

function len(o) {
	if (typeof o.length == 'undefined') {
		var c = 0;
		for(var i in o)
			++c;
		return c;
	}
	return o.length;
}

function update_css() {
	$('optionsBox').css( "background-color", WebDeveloper.themes[WebDeveloper.theme][0][3] );
	$('optionsBox').css( "borders", WebDeveloper.themes[WebDeveloper.theme][0][2] );
	$('h1').css( "color", WebDeveloper.themes[WebDeveloper.theme][0][1] );
	$('html').css( "color", WebDeveloper.themes[WebDeveloper.theme][0][0] )
	.css( "background-image", 'url("../images/backgrounds/'+WebDeveloper.optionsBackground+'")' );
}

function init() {
	if($('#badge_icon').empty()) {
		$('#theme_switcher').change(save_theme);
		$('#badge_color').change(save_badge_color);
		$('#badge_text').change(save_badge_text);
		$('#badge_icon').change(save_badge_icon);
		$('#badge_color_name').change(save_badge_color);
		$('#badge_color_vector').change(save_badge_color_vector);
		$('#background').change(save_background);
		$('#useragent_switcher').change(save_useragent);
		$('#useragents').change(save_useragents);
		$('#export_useragents').change(export_useragents);
		$('#import_useragents').click(import_useragents);
		$('#save button').click(save_options);
	}
	
	WebDeveloper.init_theme();
			
	console.log("[options][init] Current theme: " + WebDeveloper.theme);
	console.log("[options][init] Current background: " + WebDeveloper.optionsBackground);
	console.log("[options][init] Current badge icon: " + WebDeveloper.badgeIcon);
	console.log("[options][init] Current badge text: " + WebDeveloper.badgeText);
	console.log("[options][init] Current badge color: " + WebDeveloper.badgeColor);
	console.log("[options][init] Current badge color name: " + WebDeveloper.badgeColorName);
	console.log("[options][init] Current badge color vector: " + WebDeveloper.badgeColorVector);
	
	/* -*- */
	
	if (len(WebDeveloper.themes) >> 1)
	{
		console.log("[options][init] Found " + len(WebDeveloper.themes) + " themes.");
		$("#theme_switcher").val(WebDeveloper.theme);
		$('#theme_switcher').removeAttr('disabled');
	}
	
	$('#badge_color').empty();
    for(var color in WebDeveloper.badgeColors)
		$(new Option(color.replace(/_/g,' '), color)).appendTo('#badge_color');
	
	$('#theme_switcher').empty();
    for(var theme in WebDeveloper.themes)
		$(new Option(theme.replace(/_/g,' '), theme)).appendTo('#theme_switcher');
	
	//$('#badge_icon').empty();	
	if (len($("#badge_icon").children()) == 0)
		for ( var img in WebDeveloper.badgeIcons )
			$(new Option(WebDeveloper.badgeIcons[img].split(".")[0].replace(/_/g,' '), WebDeveloper.badgeIcons[img])).appendTo('#badge_icon');
	
	//$('#background').empty();	
	if (len($("#background").children()) == 0)
		for ( var img in WebDeveloper.backgrounds )
			$(new Option(WebDeveloper.backgrounds[img].split(".")[0].replace(/_/g,' '), WebDeveloper.backgrounds[img])).appendTo('#background');
	
	/* -*- */
    
	if (len(WebDeveloper.badgeColors) >> 0)
	{
		console.log("[options][init] Found " + len(WebDeveloper.badgeColors) + " badge colors.");
		$("#badge_color").val(WebDeveloper.badgeColor);
		$("#badge_color_name").val(WebDeveloper.badgeColorName);
		$("#badge_color_vector").val(WebDeveloper.badgeColorVector);
		$('#badge_color').removeAttr('disabled');
	}
	
	$("#background").val(WebDeveloper.optionsBackground);
	$("#badge_icon").val(WebDeveloper.badgeIcon);
	$("#badge_text").val(WebDeveloper.badgeText);
	
	/* -*- */
	
	current_useragent = WebDeveloper.get("wd_useragent") || navigator.userAgent;
	console.log("[options][init] Browser useragent: " + navigator.userAgent);
	console.log("[options][init] Current useragent: " + current_useragent);
	
	var useragents = WebDeveloper.get("wd_useragents", true);
	console.log("[options][init] Useragents found: "+useragents);
	$('#useragents').empty();
	if (useragents != 'null' && typeof useragents != 'undefined')
		$('#useragents').val(useragents);
		
	$('#useragent_switcher').empty();
	$(new Option(navigator.userAgent, navigator.userAgent)).appendTo('#useragent_switcher');
	
	var useragents_found = 1;
	if (useragents)
	for (var i in useragents.split("\n"))
	{
		++useragents_found;
		if (useragents.split("\n")[i].trim() != '')
			$(new Option(useragents.split("\n")[i], useragents.split("\n")[i])).appendTo('#useragent_switcher');
	}

	if (useragents_found >= 1)
	{
		console.log("[options][init] Found " + useragents_found + " useragents.");
		$('#useragent_switcher').val(current_useragent);
		$('#useragent_switcher').removeAttr('disabled');
	}
	
	/* -*- */
	
	update_css();
}

function save_theme(skip) {
	WebDeveloper.set("wd_theme", $('#theme_switcher option:selected').val(), "options");
	console.log("[options][save_theme] New theme: " + $('#theme_switcher option:selected').val());

	save_badge_text(skip);
	save_badge_icon(skip);
	save_badge_color(WebDeveloper.themes[$('#theme_switcher option:selected').val()][2], skip);
	if(typeof skip != 'bool'||!skip )
		report("Theme saved!");
}

function save_badge_icon(skip) {
	WebDeveloper.set("wd_badge_icon", $('#badge_icon').val(), "options");
	console.log("[options][save_badge_icon] New badge icon: " + WebDeveloper.get("wd_badge_icon", "options"));
	if(typeof skip != 'bool'||!skip )
		report("Badge icon saved!");
}

function save_badge_text(skip) {
	WebDeveloper.set("wd_badge_text", $('#badge_text').val(), "options");
	console.log("[options][save_badge_text] New badge text: " + $('#badge_text').val());
	if(typeof skip != 'bool'||!skip )
		report("Badge text saved!");
}

function save_badge_color(color,skip) {
	if (typeof color == 'string') {
		WebDeveloper.set("wd_badge_color", color, "options");
		WebDeveloper.set("wd_badge_color_name", color, "options");
	} else {
		if ($('#badge_color').val() != WebDeveloper.get("wd_badge_color", false))
			WebDeveloper.set("wd_badge_color_name", $('#badge_color').val(), "options");
		else WebDeveloper.set("wd_badge_color_name", $('#badge_color_name').val(), "options");
		WebDeveloper.set("wd_badge_color", $('#badge_color').val(), "options");
	}
	console.log("[options][save_badge_color] New badge color: " + WebDeveloper.get("wd_badge_color", false) + " with name: " + WebDeveloper.get("wd_badge_color_name", false));
	save_badge_color_vector(WebDeveloper.badgeColors[WebDeveloper.get("wd_badge_color_name", false)], skip);
	
	if(typeof skip != 'bool'||!skip)
		report("Badge color saved!");
}

function save_badge_color_vector(vector, skip) {
	if (!(vector instanceof Array))
	{
		var vector = new Array();
		for(var v in $('#badge_color_vector').val().split(","))
			vector.push(parseInt($('#badge_color_vector').val().split(",")[v]));
	}
	WebDeveloper.set("wd_badge_color_vector", vector, "options");
	$("#badge_color_vector").val(vector);
	
	console.log("[options][save_badge_color_vector] New badge color vector: " + WebDeveloper.get("wd_badge_color_vector", false));
	
	if(typeof skip != 'bool'||!skip)
		report("Badge color vector saved!");
}

function save_useragent(skip) {
    WebDeveloper.set("wd_useragent", $('#useragent_switcher option:selected').val(), "options");

	console.log("[options][save_useragent] New useragent: " + WebDeveloper.get("wd_useragent", false));
	if(typeof skip != 'bool'||!skip)
		report("Useragent saved!");
}

function save_useragents(skip) {
    WebDeveloper.set("wd_useragents", $('#useragents').val(), "options");
	console.log("[options][save_useragents] New useragents: " + $('#useragents').val());
	if(typeof skip != 'bool'||!skip)
		report("Useragents saved!");
	if ( new String(WebDeveloper.get("wd_useragent", false)).blank() )
		WebDeveloper.set("wd_useragent", '', "options");
}

function save_background(skip) {
	WebDeveloper.set("wd_options_background", $('#background').val(), "options");
	console.log("[options][save_background] New background: " + WebDeveloper.get("wd_options_background", "options"));
	if(typeof skip != 'bool'||!skip )
		report("Background saved!");
}

function save_options() {
	save_theme(true);
	save_useragent(true);
	save_useragents(true);
	report("Options saved!");
}